#!/usr/bin/env python3
import config, json, requests
from requests.exceptions import HTTPError

def get_mailbox(mailbox):
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json; charset=utf8'
    try:
        r = requests.get(api_base + 'get/mailbox/' + mailbox, headers=headers) 
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()

def does_mailbox_exist(mailbox):
    mailbox_info = get_mailbox(mailbox)
    return True if mailbox_info else False

def generate_mailbox_config(local_part='test', password='esdvsadfdrrewfgrfdds', full_name='test me'):
    mailbox_config = {}
    mailbox_config['active'] = '1'
    mailbox_config['domain'] = 'mail.coop'
    mailbox_config['local_part'] = local_part
    mailbox_config['name'] = full_name
    mailbox_config['password'] = password
    mailbox_config['password2'] = password
    mailbox_config['quota'] = '3072'
    mailbox_config['force_pw_update'] = '1'
    mailbox_config['tls_enforce_in'] = '0'
    mailbox_config['tls_enforce_out'] = '0'
    mailbox_config['tags'] = []
    return mailbox_config

def add_mailbox(mailbox_config):
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json'
    headers['accept'] = 'application/json'
    try:
        r = requests.post(api_base + 'add/mailbox', headers=headers, data=json.dumps(mailbox_config)) 
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()

def generate_alias_config(active="1", address="alias@example.org", goto="destination@example.com", sogo_visible="1"):
    return {"active": active, "address": address, "goto": goto, "sogo_visible": sogo_visible}

def add_alias(alias_config):
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json; charset=utf8'
    try:
        r = requests.post(api_base + 'add/alias', headers=headers, data=json.dumps(alias_config)) 
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()

def generate_user_acl_config(mailbox):
    acl_config = {} 
    attr = {} 
    user_acl = [
      "spam_score",
      "spam_policy",
      "syncjobs",
      "quarantine",
      "sogo_profile_reset",
      "quarantine_attachments",
      "quarantine_notification",
    ]
    return {"attr": {"user_acl": user_acl}, "items": mailbox}

def edit_user_acl(mailbox="foo@mail.example", user_acl_config={}):
    api_base = config.API_BASE
    headers = {"X-Api-Key": config.API_KEY}
    headers['content-type'] = 'application/json; charset=utf8'
    try:
        r = requests.post(api_base + 'edit/user-acl', headers=headers, data=json.dumps(user_acl_config)) 
        r.raise_for_status()
    except HTTPError as http_err:
        raise ValueError(f'HTTP error occurred: {http_err}')
    except Exception as err:
        raise ValueError(f'Other error occurred: {err}')
    return r.json()

def main():
    bob_mailbox = generate_mailbox_config(local_part = 'bob', full_name = 'bob banana', password = 'asdfasdfasdfasdfasdf')
    print(add_mailbox(bob_mailbox))
    bob_alias = generate_alias_config (address="bob@test.example", goto="bob@mail.coop")
    print(add_alias(bob_alias))
    bob_acl_config = generate_user_acl_config('bob@mail.coop')
    print(edit_user_acl(mailbox='bob@mail.coop',user_acl_config=bob_acl_config))
    print(edit_user_acl(mailbox='bob@mail.coop'))


if __name__ == "__main__" :
    main()

